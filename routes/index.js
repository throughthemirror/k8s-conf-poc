const express = require('express');

const router = express.Router();

router.get('/', (req, res, next) => {
    res.render('index', {
        title: 'Express',
        hostname: process.env.HOSTNAME,
        config: req.app.get('config')
    });
});

module.exports = router;
