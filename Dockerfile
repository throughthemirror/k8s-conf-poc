FROM node:8.9-alpine
ENV NODE_ENV production
WORKDIR /usr/src/app
COPY ["package.json", "package-lock.json*", "npm-shrinkwrap.json*", "./"]
COPY ["lib", "lib/"]
RUN npm install --production
COPY . .
ENV PORT=9000
EXPOSE 9000
CMD npm start