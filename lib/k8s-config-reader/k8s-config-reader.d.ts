export interface K8sConfig {
    namespace: 'auto' | 'default' | string;
    configmap: string;
    key: string;
}

export interface GetConfigOptions {
    type: 'k8s' | 'file';
    fileName?: string;
    k8sConfig?: K8sConfig;
    format?: 'yaml' | 'json' | 'plain';
}

/**
 * Get config
 *
 * @param options options
 */
export declare function getConfig(options: GetConfigOptions): Promise<any>;

/**
 * Get config assuming that the following environment variables are set:
 *  K8S_CONFIG_TYPE,
 *  K8S_CONFIG_FILENAME,
 *  K8S_CONFIG_CONFIGMAP,
 *  K8S_CONFIG_NAMESPACE,
 *  K8S_CONFIG_KEY
 */
export declare function getConfigFromEnv({ format: string } = {}): Promise<any>;
