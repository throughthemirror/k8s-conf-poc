const assert = require('assert');
const YAML = require('yaml');
const fs = require('q-io/fs');
const { KubeConfig } = require('@kubernetes/client-node/dist/config');
const { Core_v1Api } = require('@kubernetes/client-node/dist/api');

const getConfigFromFile = async fileName => {
    return fs.read(fileName);
};

const getConfigFromK8S = async ({ namespace, configmap, key }) => {
    const kConf = new KubeConfig();
    kConf.loadFromDefault();
    const api = kConf.makeApiClient(Core_v1Api);
    try {
        const res = await api.readNamespacedConfigMap(configmap, namespace);
        return res.body.data[key];
    } catch (e) {
        if (e.body) {
            throw e.body;
        }
        throw e;
    }
};

const parse = (format, data) => {
    switch (format) {
        case 'yaml':
            return YAML.parse(data);
            break;
        case 'json':
            return JSON.parse(data);
            break;
        case 'plain':
            return data;
            break;

        default:
            throw new TypeError(
                `Unknown format '${format}'. Should be one of 'yaml', 'json' or 'plain'`
            );
            break;
    }
};

const getConfigFromEnv = async ({ format = 'yaml' } = {}) => {
    const {
        env: {
            K8S_CONFIG_TYPE,
            K8S_CONFIG_FILENAME,
            K8S_CONFIG_CONFIGMAP,
            K8S_CONFIG_NAMESPACE,
            K8S_CONFIG_KEY
        }
    } = process;
    assert.ok(['k8s', 'file'].includes(K8S_CONFIG_TYPE), 'K8S_CONFIG_TYPE');
    if (K8S_CONFIG_TYPE === 'file') {
        assert.ok(K8S_CONFIG_FILENAME, 'K8S_CONFIG_TYPE');
    }
    if (K8S_CONFIG_TYPE === 'k8s') {
        assert.ok(K8S_CONFIG_CONFIGMAP, 'K8S_CONFIG_CONFIGMAP');
        assert.ok(K8S_CONFIG_NAMESPACE, 'K8S_CONFIG_NAMESPACE');
        assert.ok(K8S_CONFIG_KEY, 'K8S_CONFIG_KEY');
    }

    return getConfig({
        type: K8S_CONFIG_TYPE,
        fileName: K8S_CONFIG_FILENAME,
        format,
        k8sConfig: {
            configmap: K8S_CONFIG_CONFIGMAP,
            namespace: K8S_CONFIG_NAMESPACE,
            key: K8S_CONFIG_KEY
        }
    });
};

const getConfig = async ({ type, fileName, k8sConfig, format = 'yaml' }) => {
    if (type === 'file') {
        return parse(format, await getConfigFromFile(fileName));
    } else if (type === 'k8s') {
        return parse(format, await getConfigFromK8S(k8sConfig));
    } else {
        throw new Error(`Invalid config type "${type}"`);
    }
};

exports.getConfig = getConfig;
exports.getConfigFromEnv = getConfigFromEnv;
