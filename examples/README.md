## Installation

1. Install and start Minikube

2. Create the demo environment:

```bash
kubectl apply -f ./examples/k8s-environment.yaml
```


## 1. Kubernetes Demo


Get the URL of the demo server:

```bash
minikube service frontend-node-server --url
```


## 2. CLI Demo

```bash
node ./examples/command-line-client.js
```

## 2. Local server getting the config remotely from Kubernetes

```
./examples/server-with-envs
```
