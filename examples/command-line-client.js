const YAML = require('yaml');
const reader = require('k8s-config-reader');

const main = async () => {
    try {
        const conf = await reader.getConfig({
            type: 'k8s',
            k8sConfig: {
                namespace: 'default',
                configmap: 'server-config',
                key: 'server.properties'
            }
        });
        console.log(JSON.stringify(conf, false, 2));
    } catch (e) {
        console.error(e.stack ? e.stack : e);
        process.exit(1);
    }
};

main();
